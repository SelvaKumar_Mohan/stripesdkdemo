package com.example.stripesdkdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.view.CardInputWidget;
import com.stripe.android.view.CardNumberEditText;

public class MainActivity extends AppCompatActivity {

    String TAG = MainActivity.class.getSimpleName();

    CardInputWidget cardInputWidget;
    Button revealCardDataButton;
    TextView cardData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cardInputWidget = findViewById(R.id.card_input_widget);
        cardData = findViewById(R.id.txt_card_data);
        revealCardDataButton = findViewById(R.id.btn_reveal_card_data);
        revealCardDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FrameLayout frameLayout = (FrameLayout) cardInputWidget.getChildAt(1);
                TextInputLayout textInputLayout = (TextInputLayout) frameLayout.getChildAt(0);
                FrameLayout frameLayout2 = (FrameLayout) textInputLayout.getChildAt(0);
                CardNumberEditText cardNumberEditText = (CardNumberEditText) frameLayout2.getChildAt(0);
                Log.d(TAG, "onClick: CardNumber "+cardNumberEditText.getCardNumber());
                cardData.setText("CardNumber : "+cardNumberEditText.getText());
            }
        });

    }
}
